#include "mainwindow.h"

#include <QVBoxLayout>
#include <QMenuBar>
#include <QMenu>
#include <QDebug>
#include <QFileDialog>
#include <QFile>
#include <QStatusBar>
#include <QMessageBox>
#include <QTextCursor>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    showMaximized();

    // create interface

    _textEdit = new QPlainTextEdit(this);
    _textEdit->setEnabled(false);

    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainLayout->addWidget(_textEdit);

    QWidget* mainWidget = new QWidget();
    mainWidget->setLayout(mainLayout);

    setCentralWidget(mainWidget);

    QMenu* menuFile = menuBar()->addMenu("File");

    _actionOpen = new QAction(tr("Open"));
    _actionOpen->setShortcut(QKeySequence::Open);
    _actionSave = new QAction(tr("Save"));
    _actionSave->setShortcut(QKeySequence::Save);
    _actionSaveAs = new QAction(tr("Save as"));
    _actionSaveAs->setShortcut(QKeySequence::SaveAs);

    menuFile->addActions({_actionOpen, _actionSave, _actionSaveAs});

    connect(_actionOpen, &QAction::triggered, this, &MainWindow::onOpen);
    connect(_actionSave, &QAction::triggered, this, &MainWindow::onSave);
    connect(_actionSaveAs, &QAction::triggered, this, &MainWindow::onSaveAs);
    connect(_textEdit, &QPlainTextEdit::cursorPositionChanged, this, &MainWindow::onCursorChanged);

    _isFileModifyed = false;
    statusBar()->clearMessage();

}

MainWindow::~MainWindow()
{

}

void MainWindow::onOpen()
{
    // When we open file we show open file dialog for choosing file

    disconnect(_textEdit, &QPlainTextEdit::textChanged, this, &MainWindow::onTextChanged);

    _currentFileName = QFileDialog::getOpenFileName(this,
        tr("Open file"), "/home", tr("Text Files (*.txt)"));

    // open file and append it in text edit

    QFile file(_currentFileName);

    if(!file.open(QIODevice::ReadOnly))
        return;

    _textEdit->appendPlainText(file.readAll());

    file.close();

    setWindowTitle(file.fileName()); // set file path to title
    _textEdit->setEnabled(true);
    _textEdit->moveCursor(QTextCursor::MoveOperation::Start); // scroll document to start position
    _isFileModifyed = false;

    connect(_textEdit, &QPlainTextEdit::textChanged, this, &MainWindow::onTextChanged);
}

void MainWindow::onSave()
{
    // save text to file

    QFile file(_currentFileName);

    if(!file.open(QIODevice::WriteOnly))
        return;

    file.write(_textEdit->toPlainText().toLocal8Bit());
    file.close();

    // clear status

    _currentStatus = "";
    statusBar()->clearMessage();
    _isFileModifyed = false;
}

void MainWindow::onSaveAs()
{
    // show save file dialog

    QString newName = QFileDialog::getSaveFileName(this, tr("Save As"), "/home", "*.txt");

    // check if user cancel operation
    if(newName.isEmpty())
        return;

    // save to file

    QFile file(newName);

    if(!file.open(QIODevice::WriteOnly))
        return;

    file.write(_textEdit->toPlainText().toLocal8Bit());

    file.close();

    setWindowTitle(file.fileName()); // set new file name to title
    _currentFileName = newName;
    _currentStatus = ""; // clear status
    statusBar()->clearMessage();
    _isFileModifyed = false;

}

void MainWindow::onTextChanged()
{
    // if text was changed show status in statusbar

    _currentStatus = "Text Changed";
    statusBar()->showMessage(QString("%1 %2").arg(_cursorPosition).arg(_currentStatus));

    _isFileModifyed = true;
}

void MainWindow::onCursorChanged()
{
    // show cursor position in statusbar

    QTextCursor cursor = _textEdit->textCursor();
    _cursorPosition = QString("[row:%1, pos:%2]").arg(cursor.blockNumber()).arg(cursor.positionInBlock());

    statusBar()->showMessage(QString("%1 %2")
                             .arg(_cursorPosition)
                             .arg(_currentStatus));
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    Q_UNUSED(e);

    askToSave();

    close();

}

void MainWindow::askToSave()
{
    if(_isFileModifyed) {
        // if file was changed - ask to save it

        QMessageBox msgBox;
        msgBox.setText("File was changed. Do you want save it?");
        msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
        int result = msgBox.exec();

        if(result == QMessageBox::Ok) {
            // if user want to save file
            onSave();

        }
    }
}
