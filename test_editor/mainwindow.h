#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPlainTextEdit>
#include <QAction>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onOpen(); // open file
    void onSave(); // save text in file
    void onSaveAs(); // save text in new file
    void onTextChanged(); // show statusbar message if text was changed
    void onCursorChanged(); // show cursor position

private:
    void closeEvent(QCloseEvent* e) override;
    void askToSave(); // show message before close

private:
    QPlainTextEdit* _textEdit;
    QAction* _actionOpen;
    QAction* _actionSave;
    QAction* _actionSaveAs;
    QString _currentFileName; // file name
    bool _isFileModifyed; // we havt to remember if file was modifyed
    QString _currentStatus; // status
    QString _cursorPosition; // cursor position

};
#endif // MAINWINDOW_H
